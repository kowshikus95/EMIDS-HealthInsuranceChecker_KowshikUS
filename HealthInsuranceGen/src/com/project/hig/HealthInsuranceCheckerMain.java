package com.project.hig;

import com.project.hig.exceptions.CustomException;
import com.project.hig.mocks.PatientMocks;

public class HealthInsuranceCheckerMain {

	public static void main(String[] args) {

		try {
			HealthInsuranceCheckerService.getPatientInsuranceAmt(PatientMocks.getAboveAgeMalePatientExceedsAllLimits());
			HealthInsuranceCheckerService.getPatientInsuranceAmt(PatientMocks.getNormalUnderAgeNonMalePatient());
			
		} catch (CustomException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
