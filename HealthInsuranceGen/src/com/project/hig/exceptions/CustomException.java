package com.project.hig.exceptions;

public class CustomException extends Exception{

	private static final long serialVersionUID = 1L;
	String message;
	public CustomException(String s){  
		  super(s);  
		  message = s;
	}
	public String getMessage() {
		return message;
	}
	
}
