package com.project.hig.mocks;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import com.project.hig.enums.GenderEnum;
import com.project.hig.enums.HabitEnum;
import com.project.hig.enums.PreConditionEnum;
import com.project.hig.model.CurrentHabit;
import com.project.hig.model.CurrentHealth;
import com.project.hig.model.PatientInput;

public class PatientMocks {

public static PatientInput getAboveAgeMalePatientExceedsAllLimits() {
	PatientInput patient = new PatientInput();
	patient.setName("malePatient");
	try {
		patient.setDob(new SimpleDateFormat("dd/MM/yyyy").parse("11/11/1989"));
	} catch (ParseException e) {
		e.printStackTrace();
	}
	patient.setGender(GenderEnum.MALE.getDisplayText());
	
	List<CurrentHabit> habits = new ArrayList<CurrentHabit>();
	habits.add(new CurrentHabit(HabitEnum.SMOKING, true));
	habits.add(new CurrentHabit(HabitEnum.ALCOHOL, true));
	habits.add(new CurrentHabit(HabitEnum.EXERCISE, false));
	habits.add(new CurrentHabit(HabitEnum.DRUGS, true));
	
	patient.setCurrentHabitStack(habits);
			
	List<CurrentHealth> healths = new ArrayList<CurrentHealth>(); 
	healths.add(new CurrentHealth(PreConditionEnum.HYPERTENSION.getDisplayText(), true));
	healths.add(new CurrentHealth(PreConditionEnum.BP.getDisplayText(), true));
	healths.add(new CurrentHealth(PreConditionEnum.BLOODSUGAR.getDisplayText(), true));
	healths.add(new CurrentHealth(PreConditionEnum.OVERWEIGHT.getDisplayText(), true));
	
	patient.setCurrentHealthStack(healths);

	return patient;
	
}

public static PatientInput getNormalUnderAgeNonMalePatient() {
	PatientInput patient = new PatientInput();
	patient.setName("normalPatient");
	try {
		patient.setDob(new SimpleDateFormat("dd/MM/yyyy").parse("11/11/2010"));
	} catch (ParseException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	patient.setGender(GenderEnum.FEMALE.getDisplayText());
	
	List<CurrentHabit> habits = new ArrayList<CurrentHabit>();
	habits.add(new CurrentHabit(HabitEnum.SMOKING, false));
	habits.add(new CurrentHabit(HabitEnum.ALCOHOL, false));
	habits.add(new CurrentHabit(HabitEnum.EXERCISE, false));
	habits.add(new CurrentHabit(HabitEnum.DRUGS, false));
	
	patient.setCurrentHabitStack(habits);
	
	List<CurrentHealth> healths = new ArrayList<CurrentHealth>(); 
	healths.add(new CurrentHealth(PreConditionEnum.HYPERTENSION.getDisplayText(), false));
	healths.add(new CurrentHealth(PreConditionEnum.BP.getDisplayText(), false));
	healths.add(new CurrentHealth(PreConditionEnum.BLOODSUGAR.getDisplayText(), false));
	healths.add(new CurrentHealth(PreConditionEnum.OVERWEIGHT.getDisplayText(), false));
	
	patient.setCurrentHealthStack(healths);
	
	return patient;
	
}
}
