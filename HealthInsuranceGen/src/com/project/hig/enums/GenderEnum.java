package com.project.hig.enums;

public enum GenderEnum {

	MALE(1,"Male"),FEMALE(2,"Female"),OTHER(3,"Other");
	
	private int index;
	private String displayText;
	
	GenderEnum(int idx,String display){
		index= idx;
		displayText= display;
	}

	public int getIndex() {
		return index;
	}

	public String getDisplayText() {
		return displayText;
	}
	
	
}
